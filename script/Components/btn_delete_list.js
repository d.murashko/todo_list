import {Component} from "./component.js";

export class ButtonDeleteList extends Component{
    constructor() {
        super();
        this._createLayout();
    }

    _createLayout() {
        this._element = document.createElement('button');
        this._element.classList.add('trello_tasks-list-btn-delete-list');
        this._element.innerText = 'Delete List';
    }
}