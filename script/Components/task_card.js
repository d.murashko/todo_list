import {Component} from "./component.js";

export class TaskCardContainer extends Component {
    _taskCard;
    _taskText;
    _buttonDestroyTaskCard;

    constructor() {
        super();
        this._createLayout();
        this._dragOver(this._element);
    }

    _createLayout() {
        this._element = document.createElement('div');
        this._element.classList.add('trello_tasks-card-container');
    }

    _createTaskCard(val) {
        this._taskCard = document.createElement('div');
        this._taskCard.classList.add('trello_tasks-card');
        this._taskCard.setAttribute('draggable', 'true');

        this._taskText = document.createElement('p');
        this._taskText.innerText = `${val}`;
        this._taskCard.append(this._taskText);

        this._element.append(this._taskCard);
    }

    _createTaskCardDestroyerButton() {
        this._buttonDestroyTaskCard = document.createElement('button');
        this._buttonDestroyTaskCard.classList.add('trello_tasks-card-destroyer-btn');
        this._buttonDestroyTaskCard.innerText = 'X';
        this._taskCard.append(this._buttonDestroyTaskCard);
    }

    _destroyTaskCardListener() {
        this._element.addEventListener('click', (e) => {
            e.preventDefault();
            if (e.target.classList.contains('trello_tasks-card-destroyer-btn')) {
                e.target.parentNode.remove();
            }
        });
    }

    sortTasksCard() {
        let listArray = [];
        this._element.childNodes.forEach(item => {listArray.push(item)});
        listArray.sort((a, b) => (a.innerText.toLowerCase() > b.innerText.toLowerCase()) ? 1 : -1 );
        this._element.innerHTML = '';
        listArray.forEach(item => this._element.append(item));
    }



    //Drag and drop events

    _dragStart(item) {
        item.addEventListener('dragstart', () => {
            item.classList.add('over')
        });
    }

    _dragEnd(item) {
        item.addEventListener('dragend', () => {
            item.classList.remove('over')
        });
    }

    _dragOver(elem) {
        elem.addEventListener('dragover', (e) => {
            e.preventDefault();
            const afterElement = this.getDragAfterElement(elem, e.clientY);
            const draggable = document.querySelector('.over');
            if (afterElement == null) {
                elem.appendChild(draggable)
            } else {
                elem.insertBefore(draggable, afterElement)
            }
        });
    }

    getDragAfterElement(container, y) {

        const draggableElements = [...container.querySelectorAll('.trello_tasks-card:not(.over)')];

        return draggableElements.reduce((closest, child) => {
            const box = child.getBoundingClientRect();
            const offset = y - box.top - box.height / 2;
            if (offset < 0 && offset > closest.offset) {
                return { offset: offset, element: child }
            } else {
                return closest
            }
        }, { offset: Number.NEGATIVE_INFINITY }).element;
    }

    addDragAndDropToTasksCard() {
        [].forEach.call(this._element.childNodes, (item) => {
            this._dragStart(item);
            this._dragEnd(item);
        });
    }

}