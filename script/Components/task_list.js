import {Component} from "./component.js";
import {HeaderTaskList} from './header_task_list.js';
import {FormCreateTaskCard} from './form_create_task.js';
import {ButtonShowCreatorTask} from './btn_show_creator_task.js';
import {TaskCardContainer} from './task_card.js';
import {ButtonDeleteList} from './btn_delete_list.js';

export class TasksList extends Component{
    _header;
    _containerTasks;
    _containerFormCreator;
    _btnShowTaskCardForm;

    constructor(val) {
        super();
        this._header = new HeaderTaskList(val);
        this._containerTasks = new TaskCardContainer();
        this._containerFormCreator = new FormCreateTaskCard();
        this._btnShowTaskCardForm = new ButtonShowCreatorTask();
        this._btnDeleteList = new ButtonDeleteList();
        this._createLayout();
        this._openTaskCardFormListener();
        this._sortTasksCardListener();
        this._deleteListListener();
    }

    _createLayout() {
        this._element = document.createElement('div');
        this._element.classList.add('trello_tasks-list');
        this._header.appendTo(this._element);
        this._containerTasks.appendTo(this._element);
        this._containerFormCreator.appendTo(this._element);
        this._btnShowTaskCardForm.appendTo(this._element);
        this._btnDeleteList.appendTo(this._element);
    }

    _openTaskCardFormListener() {
        this._btnShowTaskCardForm._element.addEventListener('click', (e) => {
            e.preventDefault();

            if (this._containerFormCreator._element.innerHTML === '') {
                this._containerFormCreator._createTextareaTaskCard();
                this._containerFormCreator._createButtonAcceptTaskCard();
                this._containerFormCreator._createButtonCancelTaskForm();
                this._containerFormCreator._cancelFormListener();
                this._acceptNewTask();
            }
        });
    }

    _acceptNewTask() {
        this._containerFormCreator._buttonAcceptTaskCard.addEventListener('click', (e) => {
            e.preventDefault();
            if(this._containerFormCreator._formTask.value !== '')  {
                this._containerTasks._createTaskCard(this._containerFormCreator._formTask.value);
                this._containerTasks._createTaskCardDestroyerButton();
                this._containerTasks.addDragAndDropToTasksCard();
                this._containerTasks._destroyTaskCardListener();
                this._containerFormCreator._element.innerHTML = '';
            } else {
                alert('Type something');
            }
        });
    }

    _sortTasksCardListener() {
        this._header._btnSortList.addEventListener('click', (e) => {
            e.preventDefault();
            this._containerTasks.sortTasksCard();
        });
    }

    _deleteListListener() {
        this._btnDeleteList._element.addEventListener('click', (e) => {
            e.preventDefault();
            this._element.remove();
        });
    }
}