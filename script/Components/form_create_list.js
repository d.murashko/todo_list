import {Component} from "./component.js";

export class FormCeateTasksList extends Component{
    _nameList;
    _btnAddNewList;

    constructor() {
        super();
        this._createLayout();
    }

    _createLayout() {
        this._element = document.createElement('div');
        this._element.classList.add('trello_create-list-container');

        this._nameList = document.createElement('input');
        this._nameList.classList.add('trello_create-list-input');
        this._nameList.setAttribute('type', 'text');
        this._nameList.setAttribute('placeholder', 'Enter name you List');
        this._element.append(this._nameList);

        this._btnAddNewList = document.createElement('button');
        this._btnAddNewList.classList.add('trello_create-list-btn-add-list');
        this._btnAddNewList.innerText = 'Add new List';
        this._element.append(this._btnAddNewList);
    }

}