import {Component} from "./component.js";

export class ButtonShowCreatorTask extends Component{
    constructor() {
        super();
        this._createLayout();
    }

    _createLayout() {
        this._element = document.createElement('button');
        this._element.classList.add('trello_tasks-list-btn-add-task');
        this._element.innerText = 'Add TASK';
    }
}