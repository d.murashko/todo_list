import {Component} from "./component.js";

export class FormCreateTaskCard extends Component {
    _formTask;
    _buttonAcceptTaskCard;
    _buttonCancelTaskForm;

    constructor() {
        super();
        this._createLayout();
    }

    _createLayout() {
        this._element = document.createElement('div');
        this._element.classList.add('trello_tasks-card-form-container');
    }

    _createTextareaTaskCard() {
        this._formTask = document.createElement('textarea');
        this._formTask.classList.add('trello_tasks-card-form');
        this._formTask.setAttribute('placeholder', 'Type something here...');
        this._element.append(this._formTask);
    }

    _createButtonAcceptTaskCard() {
        this._buttonAcceptTaskCard = document.createElement('button');
        this._buttonAcceptTaskCard.classList.add('trello_tasks-card-accept-btn');
        this._buttonAcceptTaskCard.innerText = '+';
        this._element.append(this._buttonAcceptTaskCard);
    }

    _createButtonCancelTaskForm() {
        this._buttonCancelTaskForm = document.createElement('button');
        this._buttonCancelTaskForm.classList.add('trello_tasks-card-cancel-btn');
        this._buttonCancelTaskForm.innerText = '-';
        this._element.append(this._buttonCancelTaskForm);
    }

    _cancelFormListener() {
        this._buttonCancelTaskForm.addEventListener('click', (e) => {
            e.preventDefault();
            this._element.innerHTML = '';
        });
    }
}