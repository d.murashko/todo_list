export class Component {
    _element;

    constructor() {
    }

    appendTo(container) {
        container.append(this._element);
    }
}