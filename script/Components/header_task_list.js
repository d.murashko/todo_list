import {Component} from "./component.js";

export class HeaderTaskList extends Component{
    _titleList;
    _btnSortList;

    constructor(title) {
        super();
        this._title = title;
        this._createLayout();
    }

    _createLayout() {
        this._element = document.createElement('div');
        this._element.classList.add('trello_tasks-list-header');

        this._titleList = document.createElement('p');
        this._titleList.classList.add('trello_tasks-list-title');
        this._titleList.innerText = `${this._title}`;
        this._element.append(this._titleList);


        this._btnSortList = document.createElement('button');
        this._btnSortList.classList.add('trello_tasks-list-btn-sort');
        this._btnSortList.innerText = 'Sort';
        this._element.append(this._btnSortList);
    }
}