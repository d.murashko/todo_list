**`Simple ToDo List`**

This is a list of errands and other tasks – 
that you need or intend to accomplish.

**`What was used`**

- HTML
- CSS
- Native JavaScript (on classes)
- Drag and drop effects

**`See application`**

You have to clone this project to your computer and run `index.html`

###### or

Click this page: https://d.murashko.gitlab.io/todo_list or https://di-m-todo-list.netlify.app/